.. pyDLVolumen documentation master file, created by

.. include:: ../README.rst

Usage
=====

.. sphinx_argparse_cli::
  :module: berhoel.dl_volumen
  :func: build_parser
  :prog: dl_volumen
  :no_default_values:

API documentation
=================

.. currentmodule:: berhoel

.. autosummary::
   :nosignatures:

   berhoel.dl_volumen

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   _tmp/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

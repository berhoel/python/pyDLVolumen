"""Import old volume data into sqlite3 db."""

import sqlite3
import datetime
from pathlib import Path

import numpy as np
from rich.console import Console

__date__ = "2024/08/06 00:20:57 hoel"[6:-1]
__author__ = "`Berthold Höllmann <berthold@xn--hllmanns-n4a.de>`__"
__version__ = "$Revision$"[10:-1]
__copyright__ = "Copyright © 2011 by Berthold Höllmann"

CONSOLE = Console()

TIME_FORMAT = "%d.%m.%Y %H:%M:%S"

if __name__ == "__main__":
    CONN = sqlite3.connect(Path("~/DLVolumen/DLVolumen.db").expanduser())

    CUR = CONN.cursor()

    TABLES = CONN.execute(
        "SELECT name FROM sqlite_master " "WHERE type='table'" "ORDER BY name;"
    ).fetchall()

    DATFILES = sorted(Path("/home/hoel/DLVolumen/").glob("DLVolumen_*.dat"))

    if not TABLES or "volume" not in TABLES[0]:
        # Create table
        CUR.execute("CREATE TABLE volume (ts TIMESTAMP, down REAL, up REAL)")
        CUR.execute("CREATE UNIQUE INDEX volume_timestamp ON volume (ts, down, up)")

    for datfile in DATFILES:
        CONSOLE.print(datfile)

        with datfile.open() as cdata:
            clist = np.array([list(line.split()) for line in cdata])
        ins = np.array(
            [
                (i, f"{val[1]}:00")
                for i, val in enumerate(clist)
                if len(val[1].split(":")) != 3  # noqa:PLR2004
            ]
        )

        if len(ins) != 0:
            clist[:, 1].put([int(i) for i in ins[:, 0]], ins[:, 1])

        clist = np.array(
            [
                (
                    datetime.datetime.strptime(  # noqa:DTZ007
                        " ".join(i[:2]), TIME_FORMAT
                    ),
                    float(i[2]),
                    float(i[3]),
                )
                for i in clist
            ],
            np.dtype("object"),
        )

        try:
            for data in clist:
                CUR.execute("insert into volume values (?, ?, ?)", data)
        except sqlite3.IntegrityError:
            CONSOLE.print(f"could not insert '{data}'")
        CONN.commit()

    # We can also close the cursor if we are done with it
    CUR.close()
    CONN.close()

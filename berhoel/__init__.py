"""Initialization for berhoel python namespace packages."""

__date__ = "2024/08/06 19:02:07 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2017, 2022 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

__path__ = __import__("pkgutil").extend_path(__path__, __name__)

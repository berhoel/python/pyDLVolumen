"""Check for download volume."""

from __future__ import annotations

import argparse
import datetime
import email
import email.errors
import email.header
import email.utils
from importlib import metadata
import mailbox
import os
from pathlib import Path
import re
import sqlite3
import time
from typing import IO, Any, NamedTuple

import matplotlib as mpl
import matplotlib.dates as mdates
import numpy as np
import pdbp
from rich.console import Console
import scipy.optimize  # type: ignore[import-untyped]

if os.environ.get("DISPLAY") is not None:
    mpl.use("GTK3Cairo")

import matplotlib.pyplot as plt  # isort:skip

__date__ = "2024/08/06 18:36:37 Berthold Höllmann"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2004,2005,2022 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

CAPACITY = 750 * 1024  # 100 * 1024  # 12 * 1024

MAILSOURCE = Path.home() / "Mail" / "MRH.Netzzugang.Downloadstand"

SUBJECT_MATCH = re.compile(
    r"""
Downloadstand.Berthold.Höllmann.
(?P<TimeStamp>\d{1,2}\.\d{1,2}\.\d{4}.\d{2}:\d{2})""",
    re.VERBOSE | re.UNICODE,
)

DATA_MATCH = re.compile(
    r"""
Aktuell.haben.Sie.ein.Downloadvolumen.von.(?P<Download>\d+).(Megabyte.){0,1}
und.ein.Uploadvolumen.von.(?P<Upload>\d+).Megabyte""",
    re.MULTILINE | re.VERBOSE | re.UNICODE | re.DOTALL,
)

TIME_FORMAT = "%d.%m.%Y %H:%M:%S"

DATA_DIR = Path.home() / "DLVolumen"
DATA_FILE = DATA_DIR / "DLVolumen.db"

CONSOLE = Console()


class PlotData(NamedTuple):
    """Collection of plot data."""

    s_downl: np.ndarray
    s_upl: np.ndarray
    a_d: np.ndarray
    b_d: np.ndarray
    a_u: np.ndarray
    b_u: np.ndarray
    work_data: np.ndarray


def open_connection() -> tuple:
    """Open database connection to sqlite database.

    Generate required table if neccesary.
    """
    conn = sqlite3.connect(
        str(DATA_FILE), detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
    )
    cur = conn.cursor()
    tables = conn.execute(
        "SELECT name FROM sqlite_master " "WHERE type='table' " "ORDER BY name;"
    ).fetchall()

    if not tables or "volume" not in tables[0]:
        # Create table
        cur.execute("CREATE TABLE volume " "(ts TIMESTAMP, down REAL, up REAL)")
        cur.execute("CREATE UNIQUE INDEX volume_timestamp ON " "volume (ts, down, up)")
    return conn, cur


def read_data(mailsource: Path, first_date: datetime.datetime) -> None:  # noqa:C901
    """Read data from email data."""

    def msg_factory(file_p: IO[Any]) -> mailbox.MHMessage:
        """Process message."""
        try:
            return mailbox.MHMessage(email.message_from_bytes(file_p.read()))
        except email.errors.MessageParseError:
            # Don't return None since that will stop the mailbox
            # iterator
            return mailbox.MHMessage("")

    conn, cur = open_connection()

    mbox = mailbox.MH(mailsource, msg_factory)
    for msg in mbox:
        charset = str(msg.get_charset()) or "iso-8859-15"
        subject = SUBJECT_MATCH.match(
            " ".join(
                [str(i[0], charset) for i in email.header.decode_header(msg["subject"])]
            )
        )
        if subject:
            dta = time.strptime(subject.group("TimeStamp"), "%d.%m.%Y %H:%M")
            if dta is None:
                raise TypeError
            dtup = list(dta)
            dtup[6] = 0
            dtup[7] = 0
            daylight_saving = dtup[8]
            msg_time_1 = datetime.datetime.fromtimestamp(  # noqa:DTZ006
                time.mktime(tuple(dtup))  # type: ignore[arg-type]
            )
            dta2 = email.utils.parsedate(msg["date"])
            if dta2 is None:
                raise TypeError
            dtup = list(dta2)
            dtup[8] = daylight_saving
            msg_time = datetime.datetime.fromtimestamp(  # noqa:DTZ006
                time.mktime(tuple(dtup))  # type: ignore[arg-type]
            )

            if abs(msg_time - msg_time_1) > datetime.timedelta(seconds=120):
                emsg = (
                    "problem with parsing time information:\n\t"
                    f'"{subject.group("TimeStamp")}" vs. "msg["date"]"'
                )
                raise ValueError(emsg)

            if msg_time > first_date:
                payload = msg.get_payload()
                if not isinstance(payload, str):
                    raise ValueError
                info = DATA_MATCH.search(payload)
                if info:
                    download, upload = (
                        float(i) for i in info.group("Download", "Upload")
                    )
                    try:  # noqa:SIM105
                        cur.execute(
                            "insert into volume values (?, ?, ?)",
                            (msg_time, download, upload),
                        )
                    except sqlite3.IntegrityError:
                        pass
                    conn.commit()
                else:
                    CONSOLE.print(payload)
                    emsg = "Error: No Volume data found in email"
                    raise ValueError(emsg)
            elif False:
                CONSOLE.print("ignored")
        else:
            CONSOLE.print(f"Not parsed message with subject:\n!{msg['subject']}!")


def model(data: float, a: np.ndarray, b: np.ndarray) -> np.ndarray:
    """Model for interpolation."""
    return np.array(a + b * data)


def gen_plot_data(
    first_of_month: datetime.datetime, last_of_month: datetime.datetime
) -> PlotData:
    """Generate plotting data."""
    _, cur = open_connection()

    work_data = cur.execute(
        "select ts, down, up from volume where " "ts >= ? and ts <= ? order by ts",
        (first_of_month, last_of_month),
    ).fetchall()

    work_data.sort()
    data = [work_data[0]]
    for i in range(1, len(work_data)):
        if work_data[i] != data[-1]:
            data.append(work_data[i])

    work_data = data

    r_data = []

    this_downl, this_upl = None, None
    _next_time, next_downl, next_upl = None, None, None
    for (this_time, this_downl, this_upl), (_next_time, next_downl, next_upl) in zip(
        work_data[:-1], work_data[1:]
    ):
        if this_downl <= next_downl and this_upl <= next_upl:
            r_data.append([this_time, this_downl, this_upl])
        else:
            CONSOLE.print(
                f"{this_downl:6.1f} <-> {next_downl:6.1f} | "
                f"{this_upl:6.1f} <-> {next_upl:6.1f}"
            )

    r_data.append([_next_time, next_downl, next_upl])

    work_data = np.array(r_data)

    seconds = [i.total_seconds() for i in (work_data[:, 0] - first_of_month)]
    q_downl = work_data[:, 1] / seconds
    s_downl = np.sum(q_downl) / float(len(q_downl))
    q_upl = work_data[:, 2] / seconds
    s_upl = np.sum(q_upl) / float(len(q_upl))

    buf_fac = float((work_data[-1, 0] - first_of_month).total_seconds())
    buf_fac /= float((last_of_month - first_of_month).total_seconds())

    CONSOLE.print(
        f"Puffer: {CAPACITY * buf_fac - work_data[-1, 1]:.1f}MB @ "
        f"{work_data[-1, 0]}"
    )

    tmp = np.array([i.total_seconds() for i in (work_data[:, 0] - first_of_month)])
    (a_d, b_d), _ = scipy.optimize.curve_fit(
        model, tmp, [float(i) for i in work_data[:, 1]]
    )

    tmp = np.array([i.total_seconds() for i in (work_data[:, 0] - first_of_month)])
    (a_u, b_u), _ = scipy.optimize.curve_fit(
        model, tmp, [float(i) for i in work_data[:, 2]]
    )

    return PlotData(s_downl, s_upl, a_d, b_d, a_u, b_u, work_data)


def gen_plot(
    first_of_month: datetime.datetime,
    last_of_month: datetime.datetime,
    plot_data: PlotData,
) -> None:
    """Generate plot."""
    hours = mdates.HourLocator(interval=8)
    days = mdates.DayLocator(interval=3)
    date_fmt = mdates.DateFormatter("%Y-%m-%d")

    fig = plt.figure()
    fig.subplots_adjust(top=0.99, bottom=0.07, left=0.05, right=0.99)
    axis = fig.add_subplot(111)

    axis.xaxis.set_major_locator(days)
    axis.xaxis.set_major_formatter(date_fmt)
    axis.xaxis.set_minor_locator(hours)

    axis.set_xlim(first_of_month, last_of_month)  # type: ignore[arg-type]
    axis.set_ylim(
        0, max(CAPACITY, plot_data.work_data[-1, 1], plot_data.work_data[-1, 2]) * 1.02
    )

    # format the coords message box
    axis.fmt_xdata = date_fmt

    axis.grid(visible=True)

    # rotates and right aligns the x labels, and moves the bottom of
    # the axes up to make room for them
    fig.autofmt_xdate()

    axis.plot(
        plot_data.work_data[:, 0],
        plot_data.work_data[:, 1],
        marker="+",
        label="Download",
    )
    axis.plot(
        plot_data.work_data[:, 0], plot_data.work_data[:, 2], marker="x", label="Upload"
    )

    pdbp.set_trace()
    axis.plot(
        np.array((first_of_month, last_of_month)),
        (CAPACITY, CAPACITY),
        label=f"{CAPACITY:d}",
    )
    axis.plot(
        np.array((first_of_month, last_of_month)),
        (5.0 / 6.0 * CAPACITY, 5.0 / 6.0 * CAPACITY),
        label=f"{5.0 / 6.0 * CAPACITY:.0f}",
    )
    axis.plot(
        np.array((first_of_month, last_of_month)),
        (2.0 / 3.0 * CAPACITY, 2.0 / 3.0 * CAPACITY),
        label=f"{2.0 / 3.0 * CAPACITY:.0f}",
    )

    handles, labels = axis.get_legend_handles_labels()
    axis.legend(handles, labels, loc=2)

    month_len = (last_of_month - first_of_month).total_seconds()
    axis.plot(np.array((first_of_month, last_of_month)), (0, CAPACITY))

    axis.plot(
        np.array((first_of_month, last_of_month)),
        np.array((0, plot_data.s_downl * month_len)),
    )
    axis.plot(
        np.array((first_of_month, last_of_month)),
        np.array(
            (
                model(0, plot_data.a_d, plot_data.b_d),
                model(month_len, plot_data.a_d, plot_data.b_d),
            )
        ),
    )
    axis.plot(
        np.array((first_of_month, last_of_month)),
        np.array((0, plot_data.s_upl * month_len)),
    )
    axis.plot(
        np.array((first_of_month, last_of_month)),
        np.array(
            (
                model(0, plot_data.a_u, plot_data.b_u),
                model(month_len, plot_data.a_u, plot_data.b_u),
            )
        ),
    )

    plt.show()


def build_parser() -> argparse.ArgumentParser:
    """Build cli parser."""
    now = datetime.datetime.now()  # noqa:DTZ005
    parser = argparse.ArgumentParser(
        prog="dl_volumen",
        description="Show used download volume for current month from emails.",
    )
    parser.add_argument(
        "month",
        type=int,
        nargs="?",
        default=now.month,
        help="month to report, default: current month.",
    )
    parser.add_argument(
        "year",
        type=int,
        nargs="?",
        default=now.year,
        help="year to report, default: current year.",
    )
    parser.add_argument(
        "--version",
        "-V",
        action="version",
        version=f"%(prog)s {metadata.version('DLVolumen')}",
    )
    return parser


def get_times() -> tuple[datetime.datetime, datetime.datetime]:
    """Get different required dates."""
    parser = build_parser()
    args = parser.parse_args()

    first_of_month = datetime.datetime(  # noqa:DTZ001
        args.year, args.month, 1, tzinfo=None
    )

    next_month = first_of_month + datetime.timedelta(days=32)
    last_of_month = datetime.datetime(  # noqa:DTZ001
        next_month.year, next_month.month, 1, tzinfo=None
    ) - datetime.timedelta(seconds=1)

    return first_of_month, last_of_month


def main() -> None:
    """Main program."""
    first_of_month, last_of_month = get_times()
    read_data(MAILSOURCE, first_of_month)
    plot_data = gen_plot_data(first_of_month, last_of_month)
    gen_plot(first_of_month, last_of_month, plot_data)


if __name__ == "__main__":
    main()

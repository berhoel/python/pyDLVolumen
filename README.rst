=============
 pyDLVolumen
=============

Studierendenwerk Hamburg provides internet access in their
dormitories. The monthly download volume is limit. This is the code I
use to keep track of the already downloaded volume.

Documentation
=============

Documentation can be found `here <https://berhoel.gitlab.io/python/pyDLVolumen/>`_
